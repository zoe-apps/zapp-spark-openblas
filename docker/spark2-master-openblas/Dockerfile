FROM ubuntu:14.04

LABEL author="Duc-Trung NGUYEN <duc-trung.nguyen@eurecom.fr>"

RUN apt-get update && apt-get install -y --force-yes software-properties-common python-software-properties
RUN apt-add-repository -y ppa:webupd8team/java
RUN /bin/echo debconf shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections

RUN apt-get update && apt-get -y install oracle-java8-installer oracle-java8-set-default curl git

ARG SPARK_VERSION
ENV SPARK_VERSION ${SPARK_VERSION:-2.1.3}

ARG HADOOP_VERSION
ENV HADOOP_VERSION ${HADOOP_VERSION:-2.6}

ENV JAVA_HOME /usr/lib/jvm/java-8-oracle/

RUN apt-get update && apt-get install -y --force-yes --no-install-recommends \
    build-essential \
    libgfortran3  \
    libatlas3-base \
    libopenblas-base \
    libblas-dev \
    liblapack3 \
    libboost-dev \
    liblapack-dev \
    libopenblas-base \
    libopenblas-dev \
    libatlas-base-dev \
    liblapacke-dev \
    wget

RUN locale-gen en_US.UTF-8

# Configure environment
ENV CONDA_DIR /opt/conda
ENV HADOOP_HOME /opt/hadoop
ENV HADOOP_CONF_DIR $HADOOP_HOME/etc/hadoop
ENV PATH $HADOOP_HOME/bin:$CONDA_DIR/bin:$PATH
ENV SHELL /bin/bash
ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8
ENV PYTHONPATH $SPARK_HOME/python:$SPARK_HOME/python/lib/py4j-0.10.4-src.zip
ENV PYSPARK_PYTHON=/opt/conda/bin/python

RUN cd /tmp && \
    mkdir -p $CONDA_DIR && \
    wget http://repo.continuum.io/miniconda/Miniconda3-4.2.12-Linux-x86_64.sh && \
    /bin/bash Miniconda3-4.2.12-Linux-x86_64.sh -f -b -p $CONDA_DIR && \
    rm Miniconda3-4.2.12-Linux-x86_64.sh && \
    $CONDA_DIR/bin/conda install --yes conda==4.2.12

# Install Python 3 packages
RUN conda install --yes \
    'pandas=0.17*' \
    'matplotlib=1.4*' \
    'scipy=0.16*' \
    'seaborn=0.6*' \
    'scikit-learn=0.16*' \
    'statsmodels=0.6.1' \
    'pillow' \
    'basemap' \
    && conda clean -yt

# install Gpy
RUN apt-get install -y python3-tk

# install gpflow
RUN git clone https://github.com/GPflow/GPflow.git
WORKDIR GPflow
RUN /opt/conda/bin/pip install .


RUN update-alternatives --config libblas.so
RUN update-alternatives --config libblas.so.3
RUN update-alternatives --config liblapack.so
RUN update-alternatives --config liblapack.so.3

# RUN apt-get install -y git
# RUN git clone https://github.com/xianyi/OpenBlas.git
# WORKDIR OpenBlas/
# RUN make clean
# RUN make -j4 BINARY=64
# RUN mkdir /usr/OpenBLAS
# RUN chmod o+w,g+w /usr/OpenBLAS/
# RUN make PREFIX=/usr/OpenBLAS install
# RUN ldconfig
# RUN ln -sf /usr/OpenBLAS/lib/libopenblas.so /usr/libblas.so
# RUN ln -sf /usr/OpenBLAS/lib/libopenblas.so /usr/libblas.so.3
# RUN ln -sf /usr/OpenBLAS/lib/libopenblas.so /usr/libblas.so.3.5
# RUN ln -sf /usr/OpenBLAS/lib/libopenblas.so /usr/liblapack.so
# RUN ln -sf /usr/OpenBLAS/lib/libopenblas.so /usr/liblapack.so.3
# RUN ln -sf /usr/OpenBLAS/lib/libopenblas.so /usr/liblapack.so.3.5

# install latest maven
RUN latest=$(curl http://www-us.apache.org/dist/maven/maven-3/ | tac | sed -ne 's/[^0-9]*\(\([0-9]\.\)\{0,3\}[0-9]\).*/\1/p' | head -1) \
	&& wget http://www-us.apache.org/dist/maven/maven-3/$latest/binaries/apache-maven-$latest-bin.tar.gz \
	&& tar -zxf apache-maven-$latest-bin.tar.gz -C /usr/local/ \
	&& ln -s /usr/local/apache-maven-$latest/bin/mvn /usr/bin/mvn

# download spark source code and build
RUN curl -s http://mirrors.ircam.fr/pub/apache/spark/spark-${SPARK_VERSION}/spark-${SPARK_VERSION}.tgz | tar -xvz -C /opt/

WORKDIR /opt
RUN ln -s spark-${SPARK_VERSION} spark

WORKDIR /opt/spark
RUN ./dev/change-version-to-2.11.sh
RUN MAVEN_OPTS="-Xmx2g -XX:MaxPermSize=512M -XX:ReservedCodeCacheSize=512m" \
    && mvn -Pyarn -Phadoop-${HADOOP_VERSION} -Dhadoop.version=${HADOOP_VERSION}.0 -Dscala-2.11 -Phive -Phive-thriftserver -Pnetlib-lgpl -DskipTests clean package

# Add Spark JARs
RUN curl http://central.maven.org/maven2/com/databricks/spark-csv_2.10/1.3.0/spark-csv_2.10-1.3.0.jar -o /opt/spark/com.databricks_spark-csv_2.10-1.3.0.jar
RUN curl http://central.maven.org/maven2/org/apache/commons/commons-csv/1.2/commons-csv-1.2.jar -o /opt/spark/org.apache.commons_commons-csv-1.2.jar
RUN curl http://central.maven.org/maven2/com/univocity/univocity-parsers/1.5.6/univocity-parsers-1.5.6.jar -o /opt/spark/com.univocity_univocity-parsers-1.5.6.jar

COPY files/spark-defaults.conf /opt/spark/conf/spark-defaults.conf

WORKDIR ~

ENV SPARK_HOME /opt/spark
ENV PATH /opt/spark/bin:/opt/spark/sbin:${PATH}

COPY files/* /opt/
RUN chmod +x /opt/*.sh
EXPOSE 8080 7077

ENV SPARK_MASTER_PORT 7077
ENV SPARK_MASTER_WEBUI_PORT 8080

CMD /opt/start-master.sh

