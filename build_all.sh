#!/usr/bin/env bash

set -e

if [ ! -d docker ]; then
  exit
fi

REPOSITORY=${REPOSITORY:-zapps}
DOCKER_REGISTRY=${DOCKER_REGISTRY:-docker-registry:5000}

VERSION=${VERSION:-`date +%Y%m%d%H%M%S`}

built_images=''
for d in `find docker -mindepth 1 -maxdepth 1 -type d -printf '%f '`; do
  pushd docker/${d}
  docker build -t ${DOCKER_REGISTRY}/${REPOSITORY}/${d}:${VERSION} .
  docker push ${DOCKER_REGISTRY}/${REPOSITORY}/${d}:${VERSION}
  popd
  built_images+="${DOCKER_REGISTRY}/${REPOSITORY}/${d}:${VERSION}\n"
done

echo "-------------END SCRIPT-----------------"
echo "Images built:"
printf ${built_images}
echo
